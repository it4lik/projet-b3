# Projet B3

Le sujet est destiné aux filières B3 Infra et B3 Cybersécu.

## Sujet

Les étudiants forment des groupes de 2 à 4 personnes, par affinité.

Le choix du sujet est laissé libre en gardant à l'esprit que l'évaluation prend en compte les critères suivants :

- challenge recherché
  - le sujet doit proposer du challenge aux étudiants
  - ne pas se contenter de surviker des choses déjà vues en cours
- pertinence de la problématique
  - le sujet est actuel
  - fait appel à des outils et techniques modernes

## Rendu attendu

### Ecrit

Un dépôt git de rendu est attendu.

Il contient au minimum un `README.md` qui présente comment utiliser votre outil/solution, il peut prendre plusieurs formes, par exemple :

- une doc d'utilisation si vous livrez un tool (du code)
- une doc d'install si vous livrez une infra
  - des fichiers d'infra-as-code
- sachez donc adapter le contenu et la forme du `README.md` en fonction de ce que vous livrez

### Oral

Les dates et heures exactes des oraux vous seront communiquées plus tard lors des sessions de soutien.

L'oral a pour but de :

- présenter votre travail
- montrer en quoi c'est quelque chose d'utile et pertinent
- prouver votre expertise sur le sujet

Une grille de notation vous sera transmise pendant les séances.
